## [0.5.3](https://gitlab.com/soul-codes/io-ts-struct/compare/0.5.2...0.5.3) (2022-01-18)

## [0.5.2](https://gitlab.com/soul-codes/io-ts-struct/compare/0.5.1...0.5.2) (2021-05-07)


### Bug Fixes

* 🐛 type an empty struct correctly ([ad32e65](https://gitlab.com/soul-codes/io-ts-struct/commit/ad32e65436a76ad5e33e2ce9278162125831cd2a))

## [0.5.1](https://gitlab.com/soul-codes/io-ts-struct/compare/0.5.0...0.5.1) (2021-04-27)


### Bug Fixes

* 🐛 add bigint as primitive type ([b84bd4b](https://gitlab.com/soul-codes/io-ts-struct/commit/b84bd4b2884ba4dde69ce7928f65a18828882b6a))
* 🐛 fix opaque leakage bug ([bcb5251](https://gitlab.com/soul-codes/io-ts-struct/commit/bcb5251c632f22072d43afacbe2103ca777c7225))

# [0.5.0](https://gitlab.com/soul-codes/io-ts-struct/compare/0.4.3...0.5.0) (2021-03-07)


### Bug Fixes

* 🐛 correctly union all echoed properties ([372f23b](https://gitlab.com/soul-codes/io-ts-struct/commit/372f23bd603d8a5959f0c6b1436242bbe8bc9ec4))


### Features

* 🎸 nullish convenient type ([7a4aa18](https://gitlab.com/soul-codes/io-ts-struct/commit/7a4aa184c6f4125bcf047cb32112ab6c93975e08))

## [0.4.3](https://gitlab.com/soul-codes/io-ts-struct/compare/0.4.2...0.4.3) (2021-01-28)


### Bug Fixes

* 🐛 allow opaque to act on decoders with nullish values ([b9e0a4b](https://gitlab.com/soul-codes/io-ts-struct/commit/b9e0a4bfd72c186085bcfd97f468fabc86fd034a))

## [0.4.2](https://gitlab.com/soul-codes/io-ts-struct/compare/0.4.1...0.4.2) (2020-12-22)


### Bug Fixes

* 🐛 correct union order semantic in modified mode ([a71239d](https://gitlab.com/soul-codes/io-ts-struct/commit/a71239d12d4a3f5ec4cbc3b096b2dfd3417f103e))

## [0.4.1](https://gitlab.com/soul-codes/io-ts-struct/compare/0.4.0...0.4.1) (2020-12-22)


### Bug Fixes

* 🐛 make union combinator respect pipe ordering semantic ([3a603dd](https://gitlab.com/soul-codes/io-ts-struct/commit/3a603ddb867ec8a1699a22930023e67314e945e3))

# [0.4.0](https://gitlab.com/soul-codes/io-ts-struct/compare/0.4.0-beta.0...0.4.0) (2020-12-15)


### Bug Fixes

* 🐛 support lazy shorthands ([036d99c](https://gitlab.com/soul-codes/io-ts-struct/commit/036d99cc2c29d85d5318718c78e0444129bc4168))

# [0.4.0-beta.0](https://gitlab.com/soul-codes/io-ts-struct/compare/0.3.2...0.4.0-beta.0) (2020-12-14)


### Features

* 🎸 opaque struct ([31c431c](https://gitlab.com/soul-codes/io-ts-struct/commit/31c431c7abe84bf61acdcab9e1ed614b7794b150))

## [0.3.2](https://gitlab.com/soul-codes/io-ts-struct/compare/0.3.1...0.3.2) (2020-12-10)


### Bug Fixes

* 🐛 pass typing tests for union of struct/primitive ([cb8a1fc](https://gitlab.com/soul-codes/io-ts-struct/commit/cb8a1fcd76d1e847dc866493451a6ad6a8b711cf))

## [0.3.1](https://gitlab.com/soul-codes/io-ts-struct/compare/0.3.0...0.3.1) (2020-12-10)


### Bug Fixes

* 🐛 tolerate primitives in union combinators ([ede6b5f](https://gitlab.com/soul-codes/io-ts-struct/commit/ede6b5f455135722cb8bcc91cf169edc98e5ec6c))

# [0.3.0](https://gitlab.com/soul-codes/io-ts-struct/compare/0.1.1...0.3.0) (2020-12-08)


### Bug Fixes

* 🐛 rewrite the API to allow multiple modifications ([4606a4d](https://gitlab.com/soul-codes/io-ts-struct/commit/4606a4d6151b8cb4b50ccfd09135bb8f856ee40b))


### BREAKING CHANGES

* 🧨 `Struct<T>` now _is_ a decoder as it directly has the `decode()` method,
and modification via paths is done with the `with()` method.

## [0.1.1](https://gitlab.com/soul-codes/io-ts-struct/compare/0.1.0...0.1.1) (2020-12-07)


### Bug Fixes

* 🐛 allow array() to accept primitive decoders ([c9dbfe8](https://gitlab.com/soul-codes/io-ts-struct/commit/c9dbfe84684be9f075dfa963f00b70d7ffa01145))

# 0.1.0 (2020-12-05)


### Features

* 🎸 add property reflection ([86a8a84](https://gitlab.com/soul-codes/io-ts-struct/commit/86a8a849bf07eeeb2398200d69996623ca22abc3))

