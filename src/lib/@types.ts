import { Decoder } from "io-ts/lib/Decoder";
import { DeepPick, DeepPickPath, Opaque } from "ts-deep-pick";

export interface Struct<A> extends Decoder<unknown, DecodedType<A>> {
  with: <K extends DeepPickPath<A> = "*">(
    ...keys: K[]
  ) => Struct<DeepPick<A, K>>;
  props: PropertyEcho<A>;
}

export type PropertyEcho<A> = UnionToIntersection<
  A extends any[] | Opaque | Primitive ? {} : { [key in UnionKeyOf<A>]: key }
> extends infer T
  ? { [key in keyof T]: T[key] }
  : never;

export type UnionKeyOf<T> = T extends infer T ? keyof T : never;

export type StructType<S extends Struct<any>> = S extends Struct<infer T>
  ? DecodedType<T>
  : never;

export type DecodedType<A> = A extends Opaque & infer A ? A : A;

export type DecoderInput<A> =
  | Decoder<unknown, A & Primitive>
  | Struct<A>
  | Struct<A & Opaque>;

export type MaybeLazy<T> = T | (() => T);

export type Primitive =
  | number
  | boolean
  | string
  | symbol
  | null
  | undefined
  | bigint;

type UnionToIntersection<T> = (T extends any ? (x: T) => any : never) extends (
  x: infer R
) => any
  ? R
  : never;
