import { pipe } from "fp-ts/lib/function";
import {
  Decoder,
  array as arrayDecoder,
  intersect as intersectDecoder,
  literal,
  union as unionDecoder,
} from "io-ts/lib/Decoder";
import { Opaque } from "ts-deep-pick/lib";

import {
  DecodedType,
  DecoderInput,
  MaybeLazy,
  PropertyEcho,
  Struct,
} from "./@types";
import { delazify, grammar, isConcrete, once } from "./internal";

export const union = <B>(b: MaybeLazy<DecoderInput<B>>) => <A>(
  a: MaybeLazy<DecoderInput<A>>
): Struct<A | B> => {
  const aa = delazify(a);
  const bb = delazify(b);
  const mod = (...keys: any[]) => {
    const A = ("with" in aa
      ? (aa as Struct<A>).with(...keys)
      : aa) as DecoderInput<any>;
    const B = ("with" in bb
      ? (bb as Struct<B>).with(...keys)
      : bb) as DecoderInput<any>;
    return union(B)(A);
  };
  const props = {
    ...("props" in aa ? aa.props : null),
    ...("props" in bb ? bb.props : null),
  };
  const decoder = once(
    () => unionDecoder(aa, bb) as Decoder<unknown, DecodedType<A | B>>
  );
  return {
    with: mod as Struct<A | B>["with"],
    props: props as PropertyEcho<A | B>,
    decode: (val) => decoder().decode(val),
  };
};

export const intersect = <A>(a: MaybeLazy<Struct<A>>) => <B>(
  b: MaybeLazy<Struct<B>>
): Struct<A & B> => {
  const aa = delazify(a) as Struct<A>;
  const bb = delazify(b) as Struct<B>;
  const mod = (...keys: any[]) => {
    const A = aa.with(...keys);
    const B = bb.with(...keys);
    return intersect(A)(B);
  };
  const props = { ...aa.props, ...bb.props };
  const decoder = once(
    () => intersectDecoder(aa)(bb) as Decoder<unknown, DecodedType<A & B>>
  );
  return {
    with: mod as Struct<A & B>["with"],
    props: props as PropertyEcho<A & B>,
    decode: (val) => decoder().decode(val),
  };
};

export const array = <A>(item: MaybeLazy<DecoderInput<A>>): Struct<A[]> => {
  const that = delazify(item);
  const mod = (...keys: string[]) =>
    array(
      "with" in that
        ? ((that as Struct<any>).with(
            ...(keys.map(extractArrayItemKeys).filter(isConcrete) as any[])
          ) as DecoderInput<any>)
        : that
    );
  const decoder = once(() => arrayDecoder<any>(that));
  return {
    with: mod as Struct<A[]>["with"],
    props: {},
    decode: (val) => decoder().decode(val),
  };

  function extractArrayItemKeys(key: string): string | null {
    return key.startsWith(grammar.array + grammar.prop)
      ? key.slice(grammar.array.length + grammar.prop.length)
      : null;
  }
};

export const opaque = <A>(
  decoder: MaybeLazy<Decoder<unknown, A>>
): Struct<(A & Opaque) | Extract<A, null | undefined>> => {
  const that = delazify(decoder as DecoderInput<A>);
  const self: Struct<A & Opaque> = {
    decode: that.decode as Decoder<unknown, DecodedType<A>>["decode"],
    with: (() => self) as Struct<A & Opaque>["with"],
    props: {} as PropertyEcho<A & Opaque>,
  };
  return self;
};

export const nullish = pipe(
  literal(null),
  union(literal(void 0 as any) as Decoder<unknown, undefined>)
);
