import { DefaultGrammar } from "ts-deep-pick/lib";

import { DecoderInput, MaybeLazy, Struct } from "./@types";
import { opaque } from "./combinators";
import { struct } from "./struct";

export const grammar: DefaultGrammar = {
  prop: ".",
  array: "[]",
  glob: "*",
  omit: "!",
  mutate: "~",
};

export function once<T>(fn: () => T) {
  let result: { value: T } | null = null;
  return () => (result || (result = { value: fn() })).value;
}

export const isConcrete = <T>(value: T | null): value is T => value != null;

export const delazify = <T>(
  decoder: MaybeLazy<DecoderInput<T>>
): DecoderInput<T> => {
  if (typeof decoder === "function") {
    return lazy(() => {
      const d = decoder();
      return "with" in d ? d : opaque(d);
    });
  }
  return decoder;
};

export const lazy = <A>(fn: () => Struct<A>): Struct<A> => {
  const deferred = once(fn);
  return {
    with: (...keys) => deferred().with(...keys),
    get props() {
      return deferred().props;
    },
    decode: (val) => deferred().decode(val),
  };
};
