import { Decoder, type } from "io-ts/lib/Decoder";
import { AmbiguousProps, DeepPickPath, DefaultGrammar } from "ts-deep-pick";

import {
  DecodedType,
  DecoderInput,
  MaybeLazy,
  PropertyEcho,
  Struct,
} from "./@types";
import { delazify, grammar, isConcrete, lazy, once } from "./internal";

export const struct = <A = {}>(
  def:
    | {
        [K in keyof A]: K extends AmbiguousProps<DefaultGrammar>
          ? never
          : MaybeLazy<DecoderInput<A[K]>>;
      }
    | (() => Struct<A>)
): Struct<A> => {
  if (typeof def === "function") return lazy(def);

  const defs: Record<string, DecoderInput<any>> = {};
  for (const prop in def) {
    defs[prop] = delazify(def[prop]);
  }

  const mod = <K extends DeepPickPath<A>>(...args: K[]) => {
    const paths = args as string[];
    const outProperties: Record<string, any> = {};
    const hasExplicitPicks = paths.some(pickSpec);
    const props = Object.keys(defs);
    const omittedProps = new Set(paths.map(omitSpec).filter(isConcrete));
    for (const prop of props) {
      if (omittedProps.has(prop)) continue;
      const specs = paths
        .map((path) => pickSpec(path) ?? mutateSpec(path))
        .filter(isConcrete)
        .filter(([outerProp]) => outerProp === prop);

      if (hasExplicitPicks && !specs.length) continue;
      const def = defs[prop];
      const subPaths = specs
        .map(([, innerProp]) => innerProp)
        .filter(isConcrete);
      outProperties[prop] = "with" in def ? def.with(...subPaths) : def;
    }
    return struct(outProperties);
  };

  const props = (() => {
    const hash: Record<string, string> = {};
    for (const prop of Object.keys(defs)) hash[prop] = prop;
    return hash;
  })();

  const decoder = once(() => type(defs) as Decoder<unknown, DecodedType<A>>);

  return {
    with: mod as Struct<A>["with"],
    props: props as PropertyEcho<A>,
    decode: (val) => decoder().decode(val),
  };
};

const getSubPath = (string: string, split: string): [string, string | null] => {
  const index = string.indexOf(split);
  return index >= 0
    ? [string.slice(0, index), string.slice(index + 1)]
    : [string, null];
};

const pickSpec = (path: string) =>
  !path.startsWith(grammar.omit) &&
  !path.startsWith(grammar.mutate) &&
  path !== grammar.glob
    ? getSubPath(path, grammar.prop)
    : null;

const mutateSpec = (path: string) =>
  path.startsWith(grammar.mutate)
    ? getSubPath(path.slice(grammar.mutate.length), grammar.prop)
    : null;

const omitSpec = (path: string) =>
  path.startsWith(grammar.omit) ? path.slice(grammar.omit.length) : null;
