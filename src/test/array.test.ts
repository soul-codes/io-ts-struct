import { isLeft, isRight } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/function";
import { number } from "io-ts/lib/Decoder";

import { array, struct } from "../lib";

test("passes the paths correctly", () => {
  const s = pipe(
    struct({
      a: number,
      b: struct({ b1: number, b2: number }),
      c: struct({ c1: number, c2: number }),
    }),
    array
  );
  const decoder = s.with("[].b.b1", "[].~c.!c2");
  expect(isRight(decoder.decode([]))).toBe(true);
  expect(isRight(decoder.decode([{ b: { b1: 10 }, c: { c1: 10 } }]))).toBe(
    true
  );
  expect(isLeft(decoder.decode({ b: { b1: 10 }, c: { c1: 10 } }))).toBe(true);
  expect(isLeft(decoder.decode([{ b: { b1: 10 } }]))).toBe(true);
});

test("tolerates primitive", () => {
  const s = pipe(number, array);
  const decoder = s.with();
  expect(isRight(decoder.decode([]))).toBe(true);
  expect(isRight(decoder.decode([1, 2]))).toBe(true);
  expect(isLeft(decoder.decode([{ obj: 1 }, 2]))).toBe(true);
  expect(isLeft(decoder.decode(["not", "number"]))).toBe(true);
});

test("accepts lazy struct", () => {
  const s = array(() => struct({ a: number, b: number }));
  const decoder = s.with("[].a");
  expect(isRight(decoder.decode([]))).toBe(true);
  expect(isRight(decoder.decode([{ a: 1 }, { a: 2 }]))).toBe(true);
  expect(isLeft(decoder.decode([{ a: 1 }, { b: 2 }]))).toBe(true);
});

test("accepts lazy decoder", () => {
  const s = array(() => number);
  const decoder = s.with();
  expect(isRight(decoder.decode([]))).toBe(true);
  expect(isRight(decoder.decode([1, 2]))).toBe(true);
  expect(isLeft(decoder.decode([{ obj: 1 }, 2]))).toBe(true);
  expect(isLeft(decoder.decode(["not", "number"]))).toBe(true);
});

test("property reflection is always empty", () => {
  const s = array(
    struct({
      a: number,
      b: number,
      c: number,
    })
  );
  expect(s.with().props).toEqual({});
});
