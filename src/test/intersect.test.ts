import { isLeft, isRight } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/function";
import { number } from "io-ts/lib/Decoder";

import { intersect, struct } from "../lib";

test("all branches of the intersection are satisfied", () => {
  const s = pipe(
    struct({
      a: number,
    }),
    intersect(
      struct({
        b: struct({ b1: number, b2: number }),
      })
    ),
    intersect(
      struct({
        c: struct({ c1: number, c2: number }),
      })
    )
  );
  const decoder = s.with("b.b1", "~c.!c2", "!a");
  expect(isRight(decoder.decode({ b: { b1: 10 }, c: { c1: 10 } }))).toBe(true);
  expect(isRight(decoder.decode({ b: { b1: 10 } }))).toBe(false);
  expect(isRight(decoder.decode({ c: { c1: 10 } }))).toBe(false);
  expect(isRight(decoder.decode({ a: 10, c: { c1: 10 } }))).toBe(false);
});

test("property reflection is merged", () => {
  const s = pipe(
    struct({
      a: number,
    }),
    intersect(
      struct({
        b: number,
      })
    ),
    intersect(
      struct({
        c: number,
      })
    )
  );
  expect(s.with().props).toEqual({ a: "a", b: "b", c: "c" });
  expect(s.with("!b").props).toEqual({ a: "a", c: "c" });
});

test("accepts lazy struct", () => {
  const s = pipe(
    struct({ a: number }),
    intersect(() => struct({ b: number })),
    intersect(struct({ c: number }))
  );
  const decoder = s.with("!c");
  expect(isRight(decoder.decode({ a: 1, b: 2 }))).toBe(true);
  expect(isLeft(decoder.decode({ a: 1, c: 2 }))).toBe(true);
});
