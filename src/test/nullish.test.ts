import { isLeft, isRight } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/function";
import { number } from "io-ts/lib/Decoder";

import { nullish, struct, union } from "../lib";

test("property reflection is kept", () => {
  const s = pipe(
    struct({
      a: number,
      b: number,
    }),
    union(nullish)
  );
  expect(s.with().props).toEqual({ a: "a", b: "b" });
  expect(s.with("!b").props).toEqual({ a: "a" });
});

test("decodes correctly", () => {
  const s = pipe(
    struct({
      a: number,
      b: number,
    }),
    union(nullish)
  );
  expect(isRight(s.decode({ a: 10, b: 20 }))).toBe(true);
  expect(isRight(s.decode(null))).toBe(true);
  expect(isRight(s.decode(void 0))).toBe(true);
  expect(isLeft(s.decode(false))).toBe(true);
  expect(isLeft(s.decode(0))).toBe(true);
  expect(isLeft(s.decode(""))).toBe(true);
  expect(isLeft(s.decode({ a: "10", b: 20 }))).toBe(true);
});
