import { isLeft, isRight, right } from "fp-ts/lib/Either";
import { number } from "io-ts/lib/Decoder";

import { struct } from "../lib";

test("pick", () => {
  const s = struct({ a: number, b: number });
  const decoder = s.with("a");
  expect(decoder.decode({ a: 10 })).toEqual(right({ a: 10 }));
  expect(isLeft(decoder.decode({ b: 10 }))).toBe(true);
});

test("omit", () => {
  const s = struct({ a: number, b: number });
  const decoder = s.with("!a");
  expect(decoder.decode({ b: 10 })).toEqual(right({ b: 10 }));
  expect(isLeft(decoder.decode({ a: 10 }))).toBe(true);
});

test("deep pick (with mutation)", () => {
  const s = struct({ a: number, b: struct({ c: number, d: number }) });
  const decoder = s.with("~b.c");
  expect(decoder.decode({ a: 10, b: { c: 10 } })).toEqual(
    right({ a: 10, b: { c: 10 } })
  );
  expect(isLeft(decoder.decode({ b: { c: 10 } }))).toBe(true);
  expect(isLeft(decoder.decode({ a: 10, b: { d: 10 } }))).toBe(true);
});

test("deep pick (exclusive)", () => {
  const s = struct({ a: number, b: struct({ c: number, d: number }) });
  const decoder = s.with("b.c");
  expect(decoder.decode({ b: { c: 10 } })).toEqual(right({ b: { c: 10 } }));
  expect(isLeft(decoder.decode({ a: 10, b: { d: 10 } }))).toBe(true);
});

test("deep omit", () => {
  const s = struct({ a: number, b: struct({ c: number, d: number }) });
  const decoder = s.with("~b.!c");
  expect(decoder.decode({ a: 10, b: { d: 10 } })).toEqual(
    right({ a: 10, b: { d: 10 } })
  );
  expect(isLeft(decoder.decode({ a: 10, b: { c: 10 } }))).toBe(true);
});

test("pick and mutations", () => {
  const s = struct({
    a: number,
    b: struct({ b1: number, b2: number }),
    c: struct({ c1: number, c2: number }),
  });
  const decoder = s.with("b.b1", "~c.!c2");
  expect(isRight(decoder.decode({ b: { b1: 10 }, c: { c1: 10 } }))).toBe(true);
  expect(isLeft(decoder.decode({ b: { b1: 10 } }))).toBe(true);
  expect(isLeft(decoder.decode({ b: { b2: 10 }, c: { c1: 10 } }))).toBe(true);
});

test("default is glob", () => {
  const s = struct({
    a: number,
    b: struct({ b1: number, b2: number }),
    c: struct({ c1: number, c2: number }),
  });
  const decoder = s.with();
  expect(
    isRight(
      decoder.decode({ a: 10, b: { b1: 10, b2: 10 }, c: { c1: 10, c2: 10 } })
    )
  ).toBe(true);
  expect(
    isLeft(decoder.decode({ b: { b1: 10, b2: 10 }, c: { c1: 10, c2: 10 } }))
  ).toBe(true);
  expect(
    isLeft(decoder.decode({ a: 10, b: { b1: 10 }, c: { c1: 10, c2: 10 } }))
  ).toBe(true);
});

test("lazy construction", () => {
  const lazy = struct(() => s);
  const s = struct({ a: number, b: number });
  const decoder = lazy.with("a");
  expect(decoder.decode({ a: 10 })).toEqual(right({ a: 10 }));
  expect(isLeft(decoder.decode({ b: 10 }))).toBe(true);
});

test("lazy property construction", () => {
  const decoder = struct({
    a: () => number,
    b: () => struct({ foo: number }),
  });
  expect(isRight(decoder.decode({ a: 1, b: { foo: 2 } }))).toBe(true);
  expect(isRight(decoder.with("a").decode({ a: 1 }))).toBe(true);
  expect(isLeft(decoder.with("!a").decode({ a: 1 }))).toBe(true);
});

test("property reflection", () => {
  const s = struct({
    a: number,
    b: number,
    c: number,
  });
  expect(s.with().props).toEqual({ a: "a", b: "b", c: "c" });
  expect(s.with("!a").props).toEqual({ b: "b", c: "c" });
  expect(s.with("a", "b").props).toEqual({ a: "a", b: "b" });
});
