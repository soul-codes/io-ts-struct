import { isLeft, isRight } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/function";
import { number } from "io-ts/lib/Decoder";
import { hasKey } from "ts-typetools/lib";

import { opaque, struct, union } from "../lib";

test("each branch of the union is staisfied", () => {
  const s = pipe(
    struct({
      a: number,
      b: struct({ b1: number, b2: number }),
    }),
    union(
      struct({
        b: struct({ b1: number }),
        c: struct({ c1: number, c2: number }),
      })
    )
  );
  const decoder = s.with("b.b1", "~c.!c2");
  expect(isRight(decoder.decode({ b: { b1: 10 } }))).toBe(true);
  expect(isRight(decoder.decode({ b: { b1: 10 }, c: { c1: 10 } }))).toBe(true);
  expect(isRight(decoder.decode({ c: { c1: 10 } }))).toBe(false);
  expect(isRight(decoder.decode({ a: 10, c: { c1: 10 } }))).toBe(false);
});

test("each branch of the nested union is staisfied", () => {
  const s = pipe(
    struct({
      a: number,
      b: struct({ b1: number, b2: number }),
    }),
    union(
      struct({
        b: struct({ b1: number }),
        c: struct({ c1: number, c2: number }),
      })
    ),
    (nested) => struct({ nested })
  );
  const decoder = s.with("nested.b.b1", "nested.~c.!c2");
  expect(isRight(decoder.decode({ nested: { b: { b1: 10 } } }))).toBe(true);
  expect(
    isRight(decoder.decode({ nested: { b: { b1: 10 }, c: { c1: 10 } } }))
  ).toBe(true);
  expect(isRight(decoder.decode({ nested: { c: { c1: 10 } } }))).toBe(false);
  expect(isRight(decoder.decode({ nested: { a: 10, c: { c1: 10 } } }))).toBe(
    false
  );
});

test("tolerates primitives", () => {
  const s = pipe(
    struct({
      a: number,
      b: struct({ b1: number, b2: number }),
    }),
    union(number)
  );
  const decoder = s.with("!a", "b.!b2");
  expect(isRight(decoder.decode({ b: { b1: 10 } }))).toBe(true);
  expect(isRight(decoder.decode(10))).toBe(true);
  expect(isLeft(decoder.decode({ a: 10 }))).toBe(true);
  expect(isLeft(decoder.decode("10"))).toBe(true);
});

test("accepts lazy struct and decoders", () => {
  const decoder = pipe(
    struct({ a: number }),
    union(() => number),
    union(() => struct({ c: number }))
  );
  expect(isRight(decoder.decode({ a: 1 }))).toBe(true);
  expect(isRight(decoder.decode({ c: 1 }))).toBe(true);
  expect(isRight(decoder.decode(1))).toBe(true);
  expect(isLeft(decoder.decode({ b: 1 }))).toBe(true);
});

test("respects pipe-combinator ordering semantic", () => {
  const before = struct({ a: number, b: number });
  const after = struct({ a: number });
  const decoder = pipe(before, union(after));
  const assertDecode = (val: unknown) => {
    const decoded = decoder.decode(val);
    if (!isRight(decoded)) throw Error("decoder error");
    return decoded.right;
  };

  // "before" structure passes before "after" structure
  pipe(assertDecode({ a: 1, b: 2 }), (x) => {
    expect(x).toHaveProperty("a", 1);
    expect(x).toHaveProperty("b", 2);
  });
  pipe(assertDecode({ a: 1, b: "2" }), (x) => {
    expect(x).toHaveProperty("a", 1);
    expect(x).not.toHaveProperty("b");
  });
  pipe(assertDecode({ a: 1 }), (x) => {
    expect(x).toHaveProperty("a", 1);
    expect(x).not.toHaveProperty("b");
  });
});

test("respects pipe-combinator ordering semantic (with mods)", () => {
  const before = struct({ a: number, b: number });
  const after = struct({ a: number });
  const decoder = pipe(before, union(after)).with("*");
  const assertDecode = (val: unknown) => {
    const decoded = decoder.decode(val);
    if (!isRight(decoded)) throw Error("decoder error");
    return decoded.right;
  };

  // "before" structure passes before "after" structure
  pipe(assertDecode({ a: 1, b: 2 }), (x) => {
    expect(x).toHaveProperty("a", 1);
    expect(x).toHaveProperty("b", 2);
  });
  pipe(assertDecode({ a: 1, b: "2" }), (x) => {
    expect(x).toHaveProperty("a", 1);
    expect(x).not.toHaveProperty("b");
  });
  pipe(assertDecode({ a: 1 }), (x) => {
    expect(x).toHaveProperty("a", 1);
    expect(x).not.toHaveProperty("b");
  });
});

test("property reflection is merged", () => {
  const s = pipe(
    struct({
      a: number,
    }),
    union(
      struct({
        b: number,
      })
    ),
    union(
      struct({
        c: number,
      })
    )
  );
  expect(s.with().props).toEqual({ a: "a", b: "b", c: "c" });
  expect(s.with("!b").props).toEqual({ a: "a", c: "c" });
});

test("property reflection is kept when merged with primitive", () => {
  const s = pipe(
    struct({
      a: number,
      b: number,
    }),
    union(number)
  );
  expect(s.with().props).toEqual({ a: "a", b: "b" });
  expect(s.with("!b").props).toEqual({ a: "a" });
});

test("property reflection is kept when merged with opaque structures", () => {
  const s = pipe(
    struct({
      a: number,
      b: number,
    }),
    union(pipe(struct({ c: number }), opaque))
  );
  expect(s.with().props).toEqual({ a: "a", b: "b" });
  expect(s.with("!b").props).toEqual({ a: "a" });
});
