import { pipe } from "fp-ts/lib/function";
import { Decoder, literal, number, string, type } from "io-ts/lib/Decoder";
import { Opaque } from "ts-deep-pick/lib";
import { ExactType, Unrelated, expectType } from "ts-typetools/lib";

import { Struct, StructType, nullish, opaque, struct, union } from "../lib";

{
  function foo<T extends string>(decoder: Decoder<unknown, T>) {
    struct({
      j: decoder,
    });
  }
}

{
  expectType(
    struct({
      a: pipe(number, union(struct({ a: number }))),
    })
  )
    .assert<Struct<{ a: number | { a: number } }>>()
    .toBe(ExactType);
}

{
  interface AB {
    a: string;
    b: string;
  }

  const s = opaque(type({ a: string, b: string }));
  expectType<StructType<typeof s>>().assert<AB>().toBe(ExactType);
  expectType<StructType<typeof s>>().assert<Opaque>().toBe(Unrelated);

  expectType(s.props).assert<{}>().toBe(ExactType);
  s.with();
  expectType<
    typeof s["with"] extends (...args: (infer K)[]) => any ? K : never
  >()
    .assert<"*">()
    .toBe(ExactType);

  const wrapped = struct({ opaqued: s });
  expectType<StructType<typeof wrapped>>()
    .assert<{ opaqued: AB }>()
    .toBe(ExactType);
  expectType<StructType<typeof wrapped>["opaqued"]>()
    .assert<Opaque>()
    .toBe(Unrelated);
}

{
  const struct = opaque(number);
  expectType<StructType<typeof struct>>().assert<number>().toBe(ExactType);

  expectType(struct.props).assert<{}>().toBe(ExactType);
  struct.with();
  expectType<
    typeof struct["with"] extends (...args: (infer K)[]) => any ? K : never
  >()
    .assert<"*">()
    .toBe(ExactType);
}

{
  const struct = pipe(number, union(literal(null)), opaque);
  expectType<StructType<typeof struct>>()
    .assert<number | null>()
    .toBe(ExactType);
}

{
  const struct = pipe(
    number,
    union(literal(void 0 as any) as Decoder<unknown, undefined>),
    opaque
  );
  expectType<StructType<typeof struct>>()
    .assert<number | undefined>()
    .toBe(ExactType);
}

{
  const s = pipe(struct({ a: number, b: number }), union(string));
  expectType<typeof s.props>().assert<{ a: "a"; b: "b" }>().toBe(ExactType);
}

{
  const s = pipe(
    struct({ a: number, b: number }),
    union(struct({ c: number }))
  );
  expectType<typeof s.props>()
    .assert<{ a: "a"; b: "b"; c: "c" }>()
    .toBe(ExactType);
}

{
  const empty = struct({});
  expectType<StructType<typeof empty>>().assert<{}>().toBe(ExactType);
}
